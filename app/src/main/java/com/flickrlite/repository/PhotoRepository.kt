package com.flickrlite.repository

import com.flickrlite.BuildConfig
import com.flickrlite.repository.model.PhotoData
import com.flickrlite.repository.model.PhotoSize
import com.flickrlite.service.api.FlickrAPI
import com.flickrlite.service.api.FlickrMethod

object PhotoRepository : BaseRepository() {

    suspend fun getPhotos(tag: String, page: Int, perPage: Int): List<PhotoData> {
        val response = safeApiCall(
            call = {
                FlickrAPI.flickrService.getPhotosAsync(
                    method = FlickrMethod.SEARCH.method,
                    tag = tag,
                    apiKey = BuildConfig.API_KEY,
                    page = page,
                    perPage = perPage
                ).await()
            },
            errorMessage = "Error to fetching images"
        )

        val result: MutableList<PhotoData> = mutableListOf()
        response?.photos?.photo?.map { it -> result.add(getPhoto(it.id)) }

        return result
    }

    suspend fun getPhoto(photoId: String): PhotoData {
        val response = safeApiCall(
            call = {
                FlickrAPI.flickrService.getPhotoDetailAsync(
                    method = FlickrMethod.GET_SIZES.method,
                    apiKey = BuildConfig.API_KEY,
                    photoId = photoId
                ).await()
            },
            errorMessage = "Error to fetching image"
        )

        val result: PhotoData
        response.let { photo ->
            val sizes = photo?.sizes?.size?.map { PhotoSize(label = it.label,
                width = it.width,
                height = it.height,
                source = it.source,
                media = it.media) }

            result = PhotoData(photoId, size = sizes!!)
        }

        return result
    }
}