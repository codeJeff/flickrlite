package com.flickrlite.repository

import android.util.Log
import com.flickrlite.service.api.FlickrResult
import retrofit2.Response
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>, errorMessage: String): T? {

        val flickrResult : FlickrResult<T> = safeApiResult(call, errorMessage)
        var data : T? = null

        when(flickrResult) {
            is FlickrResult.Success ->
                data = flickrResult.data
            is FlickrResult.Error -> {
                Log.e("BaseRepository", "$errorMessage & Exception - ${flickrResult.exception}")
            }
        }

        return data

    }

    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>, errorMessage: String) : FlickrResult<T> {
        val response = call.invoke()
        if(response.isSuccessful) return FlickrResult.Success(response.body()!!)

        return FlickrResult.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage - Error code from response ${response.raw().code()}"))
    }

}