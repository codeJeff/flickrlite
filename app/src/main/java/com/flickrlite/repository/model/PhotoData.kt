package com.flickrlite.repository.model

data class PhotoData(val id: String, val size: List<PhotoSize> = listOf(), var largeSquareImageUrl: String = "", var largeImageUrl: String = "")

data class PhotoSize(val label: String, val width: Int, val height: Int, val source: String, val media: String)