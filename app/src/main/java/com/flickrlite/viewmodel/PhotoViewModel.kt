package com.flickrlite.viewmodel

import android.app.Application
import androidx.annotation.UiThread
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.flickrlite.mechanism.Coroutines
import com.flickrlite.repository.PhotoRepository
import com.flickrlite.repository.model.PhotoData


class PhotoViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val DEFAULT_PAGE_INIT = 1
        private const val DEFAULT_TAG = "kitten"
        private const val DEFAULT_PHOTOS_PER_PAGE = 10
        private const val FILTER_TILE_LARGE_SQUARE = "Large Square"
        private const val FILTER_TILE_LARGE = "Large"
    }

    private lateinit var photos: MutableLiveData<List<PhotoData>>
    private lateinit var photosLoadMore: MutableLiveData<List<PhotoData>>

    private var page: Int = DEFAULT_PAGE_INIT

    @UiThread
    fun refreshPhotos(): LiveData<List<PhotoData>> {
        if (!::photos.isInitialized) {
            this.photos = MutableLiveData()
            this.page = DEFAULT_PAGE_INIT

            // Request data
            Coroutines.ioThenMain({
                PhotoRepository.getPhotos(DEFAULT_TAG, DEFAULT_PAGE_INIT, DEFAULT_PHOTOS_PER_PAGE)
            }) {
                it?.let { data ->
                    if (data.isNotEmpty()) {
                        val largeSquarePhotos = filterLargeSquarePhotos(data.toMutableList())
                        val photosWithImageUrl = buildPhotoUrls(largeSquarePhotos)
                        photos.value = photosWithImageUrl
                    }
                }
            }

        }
        return photos
    }

    @UiThread
    fun loadMorePhotos(): LiveData<List<PhotoData>> {
        this.photosLoadMore = MutableLiveData()
        this.page++

        // Request data
        Coroutines.ioThenMain({
            PhotoRepository.getPhotos(DEFAULT_TAG, page, DEFAULT_PHOTOS_PER_PAGE)
        }) {
            it?.let { data ->
                if (data.isNotEmpty()) {
                    val largeSquarePhotos = filterLargeSquarePhotos(data.toMutableList())
                    val photosWithImageUrl = buildPhotoUrls(largeSquarePhotos)

                    photosLoadMore.value = photosWithImageUrl
                }
            }
        }
        return photosLoadMore
    }

    private fun filterLargeSquarePhotos(photos: List<PhotoData>): List<PhotoData> {
        return photos.filter { photoData ->
            photoData.size.any {
                it.label == FILTER_TILE_LARGE_SQUARE
            }
        }
    }

    private fun buildPhotoUrls(photos: List<PhotoData>): List<PhotoData> {
        photos.map { photoData ->
            photoData.size.map {
                if (it.label == FILTER_TILE_LARGE_SQUARE) {
                    photoData.largeSquareImageUrl = it.source
                }

                if (it.label == FILTER_TILE_LARGE) {
                    photoData.largeImageUrl = it.source
                }
            }
        }
        return photos
    }

}