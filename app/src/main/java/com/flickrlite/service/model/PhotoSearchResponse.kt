package com.flickrlite.service.model

data class PhotosSearchResponse(val photos: Photos)

data class Photos(val photo: List<Photo>)

data class Photo(val id: String, val title: String, val url_s: String)