package com.flickrlite.service.model

data class PhotoSizesResponse(val sizes: PhotoSizes)

data class PhotoSizes(val size: List<PhotoSize>)

data class PhotoSize(val label: String, val width: Int, val height: Int, val source: String, val media: String)