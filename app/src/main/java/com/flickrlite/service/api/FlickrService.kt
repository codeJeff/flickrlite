package com.flickrlite.service.api

import com.flickrlite.service.model.PhotoSizesResponse
import com.flickrlite.service.model.PhotosSearchResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FlickrService {

    @GET("?format=json&nojsoncallback=1")
    fun getPhotosAsync(@Query("method") method: String,
                       @Query("tags") tag: String,
                       @Query("api_key") apiKey: String,
                       @Query("page") page: Int = 1,
                       @Query("per_page") perPage: Int = 10): Deferred<Response<PhotosSearchResponse>>

    @GET("?format=json&nojsoncallback=1")
    fun getPhotoDetailAsync(@Query("method") method: String,
                            @Query("api_key") apiKey: String,
                            @Query("photo_id") photoId: String): Deferred<Response<PhotoSizesResponse>>

}

enum class FlickrMethod(val method: String) {
    SEARCH("flickr.photos.search"),
    GET_SIZES("flickr.photos.getSizes")
}