package com.flickrlite.service.api

import java.lang.Exception

sealed class FlickrResult<out T: Any> {
    data class Success<out T : Any>(val data: T) : FlickrResult<T>()
    data class Error(val exception: Exception) : FlickrResult<Nothing>()
}