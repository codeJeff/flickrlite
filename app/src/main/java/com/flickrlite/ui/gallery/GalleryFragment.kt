package com.flickrlite.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.flickrlite.R
import com.flickrlite.mechanism.VerifyNetworkInfo
import com.flickrlite.repository.model.PhotoData
import com.flickrlite.ui.detail.DetailFragment
import com.flickrlite.viewmodel.PhotoViewModel
import kotlinx.android.synthetic.main.fragment_gallery.*


class GalleryFragment : Fragment() {

    private val viewModel: PhotoViewModel by lazy {
        ViewModelProviders.of(this).get(PhotoViewModel::class.java)
    }

    private lateinit var adapter: GalleryAdapter

    private var isLoadingMoreDone = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buildActionBar()
        buildGallery()
        bindSwipes()
        verifyConnectionState()
    }

    private fun verifyConnectionState() {
        context.let {
            if (VerifyNetworkInfo.isConnected(it!!)) {
                hideNoConnectionState()
                showProgress()
                fetchPhotos()
            } else {
                hideProgress()
                showNoConnectionState()

                state_without_conn_gallery.setOnClickListener {
                    verifyConnectionState()
                }
            }
        }
    }

    private fun buildActionBar() {
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(false) // disable the button
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false) // remove the left caret
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.title = resources.getString(R.string.app_name)
    }

    private fun buildGallery() {
        adapter = GalleryAdapter { it, imageView ->
            onClickItem(it, imageView)
        }

        gallery_rv.layoutManager = GridLayoutManager(context, 2)
        gallery_rv.itemAnimator = DefaultItemAnimator()
        gallery_rv.adapter = adapter
    }

    private fun bindSwipes() {
        getSwipeRefreshLayout().setColorSchemeResources(R.color.orange, R.color.red, R.color.green, R.color.blue)
        getSwipeRefreshLayout().setOnRefreshListener { fetchPhotos() }

        gallery_rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = gallery_rv.layoutManager?.childCount ?: 0
                    val totalItemCount = gallery_rv.layoutManager?.itemCount ?: 0
                    val pastVisibleItems =
                        (gallery_rv.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (visibleItemCount + pastVisibleItems >= totalItemCount - 2 && isLoadingMoreDone) {
                        isLoadingMoreDone = false
                        showLoadMoreProgress()
                        onLoadMore()
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val firstPos =
                    (gallery_rv.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                swipe_refresh.isEnabled = firstPos <= 0
            }
        })
    }


    private fun fetchPhotos() {
        viewModel.refreshPhotos().observe(this, Observer<List<PhotoData>> { it ->
            if (!it.isNullOrEmpty()) {
                adapter.setData(it)
                hideProgress()
                hideRefreshAnim()
                showPhotos()
            }
        })
    }

    private fun onLoadMore() {
        viewModel.loadMorePhotos().observe(this, Observer<List<PhotoData>> { it ->
            if (!it.isNullOrEmpty()) {
                adapter.addData(it)
                isLoadingMoreDone = true
                hideLoadMoreProgress()
            }
        })
    }

    private fun hideRefreshAnim() {
        if (getSwipeRefreshLayout().isRefreshing) {
            activity?.runOnUiThread {
                getSwipeRefreshLayout().isRefreshing = false
            }
        }
    }

    private fun showLoadMoreProgress() {
        activity?.runOnUiThread {
            state_progress_load_more.visibility = View.VISIBLE
        }
    }


    private fun hideLoadMoreProgress() {
        activity?.runOnUiThread {
            state_progress_load_more.visibility = View.GONE
        }
    }

    private fun getSwipeRefreshLayout() = (swipe_refresh as SwipeRefreshLayout)

    private fun showPhotos() {
        gallery_rv.visibility = View.VISIBLE
    }

    private fun showProgress() {
        state_progress_gallery.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        state_progress_gallery.visibility = View.GONE
    }

    private fun showNoConnectionState() {
        state_without_conn_gallery.visibility = View.VISIBLE
    }

    private fun hideNoConnectionState() {
        state_without_conn_gallery.visibility = View.GONE
    }

    private fun onClickItem(data: PhotoData, imageView: ImageView) {
        val extras = FragmentNavigatorExtras(
            imageView to "thumbnail"
        )
        var bundle = Bundle()
        bundle.putString(DetailFragment.KEY_PHOTO_LARGE_URL, data.largeImageUrl)
        findNavController().navigate(R.id.action_gallery_to_detail, bundle, null, extras)
    }
}