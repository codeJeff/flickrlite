package com.flickrlite.ui.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.flickrlite.R
import com.flickrlite.repository.model.PhotoData
import kotlinx.android.synthetic.main.item_gallery.view.*

class GalleryAdapter(val onClickItem: (PhotoData, ImageView) -> Unit) : RecyclerView.Adapter<GalleryItemViewHolder>() {

    private var items = mutableListOf<PhotoData>()

    fun setData(items: List<PhotoData>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addData(items: List<PhotoData>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryItemViewHolder = GalleryItemViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_gallery,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: GalleryItemViewHolder, position: Int) {
        holder.data = items[position]
        holder.view.setOnClickListener { onClickItem(items[position], holder.view.item_gallery_thumbnail) }
    }
}