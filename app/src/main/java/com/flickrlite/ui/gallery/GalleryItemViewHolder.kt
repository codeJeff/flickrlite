package com.flickrlite.ui.gallery

import android.text.TextUtils
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.flickrlite.R
import com.flickrlite.repository.model.PhotoData
import kotlinx.android.synthetic.main.item_gallery.view.*

class GalleryItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var data: PhotoData? = null
        set(value) {
            field = value
            populateThumbnail(value)
        }

    private fun populateThumbnail(data: PhotoData?) {
        data.let {
            if (!TextUtils.isEmpty(it!!.largeSquareImageUrl)) {
                Glide.with(view.context)
                    .load(it.largeSquareImageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_placeholder)
                    .into(view.item_gallery_thumbnail)
                view.item_gallery_thumbnail.visibility = View.VISIBLE
            }
        }
    }


}