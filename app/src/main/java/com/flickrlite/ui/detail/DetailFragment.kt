package com.flickrlite.ui.detail

import android.os.Bundle
import android.text.TextUtils
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.flickrlite.R
import com.flickrlite.mechanism.VerifyNetworkInfo
import kotlinx.android.synthetic.main.fragment_detail_photo.*

class DetailFragment : Fragment() {

    companion object {
        const val KEY_PHOTO_LARGE_URL = "KEY_PHOTO_LARGE_URL"
    }

    private lateinit var photoLargeUrl: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.arguments.let {
            photoLargeUrl = it?.getString(KEY_PHOTO_LARGE_URL) ?: ""
        }
        return inflater.inflate(R.layout.fragment_detail_photo, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buildActionBar()
        populateUi()
    }

    private fun buildActionBar() {
        val activity = activity as AppCompatActivity
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)
    }

    private fun populateUi() {
        verifyConnectionState()
    }

    private fun verifyConnectionState() {
        context.let {
            if (VerifyNetworkInfo.isConnected(it!!)) {
                hideNoConnectionState()
                showStateProgress()
                populateThumbnail()
            } else {
                hideStateProgress()
                showNoConnectionState()

                state_without_conn_detail.setOnClickListener {
                    verifyConnectionState()
                }
            }
        }
    }

    private fun hideStateProgress() {
        state_progress_detail.visibility = View.GONE
    }

    private fun showStateProgress() {
        state_progress_detail.visibility = View.VISIBLE
    }

    private fun showNoConnectionState() {
        state_without_conn_detail.visibility = View.VISIBLE
    }

    private fun hideNoConnectionState() {
        state_without_conn_detail.visibility = View.GONE
    }

    private fun populateThumbnail() {
        if (!TextUtils.isEmpty(photoLargeUrl)) {
            context?.let {
                Glide.with(it)
                    .load(photoLargeUrl)
                    .error(R.drawable.ic_placeholder)
                    .into(item_detail_photo_thumbnail)
                item_detail_photo_thumbnail.visibility = View.VISIBLE
                hideStateProgress()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().navigateUp()
        }
        return super.onOptionsItemSelected(item)
    }
}